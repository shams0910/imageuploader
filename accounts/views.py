from django.contrib.auth import login, logout
from django.shortcuts import redirect
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from accounts.serializers import LoginSerializer, UserSerializer

__all__ = ['LoginView']


class LoginView(GenericAPIView):
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.authenticate_user()

        if not user:
            return Response('Email or password not correct', status.HTTP_401_UNAUTHORIZED)

        login(request, user)

        refresh = RefreshToken.for_user(user)
        data = {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
            'user': UserSerializer(user).data
        }
        response = Response(data)
        return response


def log_out(request):
    """In drf I usually dont do this but for the sake of DRF browser I added that"""
    logout(request)
    return redirect('login')
