from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext, gettext_lazy as _


from accounts.models import Plan, User


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    pass


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    list_display = ('email', 'account_plan')
    ordering = ('-created_at', )
    search_fields = ('first_name', 'last_name', 'email')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'first_name', 'last_name', 'account_plan')}
         ),
    )
    fieldsets = (
        (None, {'fields': ('email', 'password', 'first_name', 'last_name', 'account_plan')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        })
    )
