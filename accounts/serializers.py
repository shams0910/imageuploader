from django.contrib.auth import authenticate
from rest_framework import serializers

from accounts.models import User


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def authenticate_user(self):
        email: str = self.validated_data['email']
        password: str = self.validated_data['password']
        return authenticate(email=email, password=password)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')
