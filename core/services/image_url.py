from datetime import datetime

from django.contrib.sites.models import Site
from django.core.signing import Signer
from django.shortcuts import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

signer = Signer()


class ImageUrlService:
    def __init__(self, image):
        self.image = image
        self.current_domain = Site.objects.get_current().domain
        self.uidb64 = urlsafe_base64_encode(force_bytes(self.image.id))

    def get_url(self,
                height: int = None,
                expires_at: datetime = None,
                is_original: bool = False) -> str:

        token_data = {
            'is_original': is_original,
            'height': height,
            'expires_at': expires_at.isoformat() if expires_at else None,
        }

        token = signer.sign_object(token_data)
        download_image_url = reverse('download_image', args=[self.uidb64, token])
        return f'http://{self.current_domain}{download_image_url}'
