from datetime import datetime

from django.utils import timezone

from accounts.models import User, Plan
from core.exceptions import UserDoesNotHavePlan
from core.models import Image
from core.services.image_url import ImageUrlService


class UserImagesService:
    """A service to get urls of images of the user based on his plan"""

    def __init__(self, user: User):
        self.user = user

    @staticmethod
    def get_image(image: Image, plan: Plan, expires_at: datetime = None):
        """Get image links according to provided plan"""
        image_url_service = ImageUrlService(image=image)

        image_item = {
            'id': image.id,
            'filename': image.filename,
            'created_at': timezone.localtime(image.created_at).strftime('%d-%m-%Y %H:%M')
        }

        if plan.has_original_image:
            image_item['original_image'] = image_url_service.get_url(is_original=True, expires_at=expires_at)

        if plan.image_sizes:
            for image_size in plan.image_sizes:
                image_item[f'size_{image_size}'] = image_url_service.get_url(height=image_size, expires_at=expires_at)

        return image_item

    def get_images(self):
        """Method to get image urls of all images of the user"""
        user_images = self.user.images.all()
        plan = self.user.account_plan

        if not plan:
            raise UserDoesNotHavePlan

        images = [self.get_image(image, plan) for image in user_images]

        return images
