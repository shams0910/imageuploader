from datetime import datetime
from PIL import Image as PImage

from django.conf import settings
from django.core.signing import Signer
from django.utils import timezone
from rest_framework.exceptions import ValidationError

from core.exceptions import ExpiredLinkException, InValidUrl
from core.models import Image

signer = Signer()


class ProcessImageService:
    def __init__(self, image: Image, token: str):
        self.image = image
        self.image_link_data = signer.unsign_object(token)

    @staticmethod
    def _get_new_size(old_size, new_height):
        old_width, old_height = old_size

        # if new_height > old_height:
        #     raise ValidationError('The requested height of the image is bigger than original height')

        ratio = new_height / old_height
        new_width = ratio * old_width
        return new_width, new_height

    def process(self):
        expires_at = self.image_link_data.get('expires_at')
        if expires_at:
            expires_at = datetime.fromisoformat(expires_at)
            if timezone.now() > expires_at:
                raise ExpiredLinkException

        if self.image_link_data['is_original']:
            return self.image.image

        height = self.image_link_data.get('height')

        if not height:
            raise InValidUrl

        path_to_image = str(settings.BASE_DIR) + self.image.image.url
        pillow_image = PImage.open(path_to_image)

        new_width, new_height = self._get_new_size(pillow_image.size, new_height=int(height))
        pillow_image.thumbnail((new_width, new_height))

        return pillow_image
