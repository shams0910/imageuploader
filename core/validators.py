import constants.core

from django.core.files.base import File
from rest_framework.exceptions import ValidationError


class LimitedFileSize:
    def __call__(self, value):
        if not isinstance(value, File):
            raise ValidationError('The uploaded file format is not correct')

        if value.size > constants.core.MAXIMUM_UPLOAD_SIZE:
            raise ValidationError(
                'The image size should be less than %s MB' % constants.core.MAXIMUM_UPLOAD_SIZE / 1024 ** 2
            )
