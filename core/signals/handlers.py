import os

from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from core.models import Image


@receiver(post_delete, sender=Image)
def auto_delete_file_on_delete(sender: Image, instance: Image, **kwargs):
    """
    Handler to delete image file when `Image` object is deleted
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)


@receiver(pre_save, sender=Image)
def auto_delete_file_on_change(sender: Image, instance: Image, **kwargs):
    """
    Handler to delete old image file when `Image` object is updated
    """
    if not instance.pk:
        return False

    try:
        old_file = Image.objects.get(pk=instance.pk).image
    except Image.DoesNotExist:
        return

    new_file = instance.image
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)
