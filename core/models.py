import os
from django.db import models

from core.db.base_models import TimeStampMixinAbstractModel

BLANK_NULL = {'blank': True, 'null': True}


def get_image_location(instance, filename):
    return f'user_{instance.user_id}/{filename}'


class Image(TimeStampMixinAbstractModel):
    image = models.ImageField(upload_to=get_image_location)
    user = models.ForeignKey('accounts.User', on_delete=models.CASCADE, related_name='images')

    @property
    def filename(self):
        return os.path.basename(self.image.name)

    @property
    def file_extension(self):
        return self.filename.split('.')[-1]

