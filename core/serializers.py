from rest_framework import serializers

import constants.core
from core.models import Image
from core.validators import LimitedFileSize


class ImageSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(validators=[LimitedFileSize(), ])

    class Meta:
        model = Image
        fields = ('id', 'image',)


class RequestExpiringLinkSerializer(serializers.Serializer):
    image = serializers.IntegerField()
    seconds = serializers.IntegerField()

    @staticmethod
    def validate_seconds(value):
        _min = constants.core.MINIMUM_EXPIRING_SECONDS
        _max = constants.core.MAXIMUM_EXPIRING_SECONDS
        if not _min <= value <= _max:
            message = f'Seconds should be in interval between {_min} and {_max}'
            raise serializers.ValidationError(message)
        return value

    def validate_image(self, value):
        try:
            self.image_instance = Image.objects.get(id=value)
        except Image.DoesNotExist:
            message = 'Requested image was not found'
            raise serializers.ValidationError(message)
        return value