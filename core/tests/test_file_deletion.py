import os
from tempfile import TemporaryDirectory

from django.test import TestCase, override_settings

from accounts.tests.factories import UserFactory
from core.tests.factories import ImageFactory


@override_settings(MEDIA_ROOT=TemporaryDirectory(prefix='media_test').name)
class FileDeletionTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserFactory.create()

    def test_auto_delete_file_on_delete(self):
        image_object = ImageFactory(user=self.user)
        image_path = image_object.image.path
        image_object.delete()
        self.assertFalse(os.path.exists(image_path))
