import factory
import os
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

from core.models import Image

test_image_path = os.path.join(settings.BASE_DIR, 'core/tests/test_image.jpg')


class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image

    image = SimpleUploadedFile(
        name='test_image.jpg',
        content=open(test_image_path, 'rb').read(),
        content_type='image/jpg'
    )