from datetime import timedelta
from tempfile import TemporaryDirectory
from unittest.mock import Mock

from django.test import TestCase, override_settings
from django.utils import timezone

from accounts.models import Plan
from accounts.tests.factories import UserFactory
from core.exceptions import ExpiredLinkException
from core.services.image_url import ImageUrlService
from core.services.process_image import ProcessImageService
from core.tests.factories import ImageFactory


@override_settings(MEDIA_ROOT=TemporaryDirectory(prefix='media_test').name)
class ExpiringLinkTestCase(TestCase):
    fixtures = ['accounts/fixtures/plans.json', ]

    @classmethod
    def setUpTestData(cls):
        plan = Plan.objects.get(name='Basic')
        user = UserFactory.create(account_plan=plan)
        cls.image = ImageFactory.create(user=user)

    def test_expired_link(self):
        # setting expiration time 300 seconds from now
        expires_at = timezone.now() + timedelta(seconds=300)
        generated_link = ImageUrlService(image=self.image).get_url(is_original=True, expires_at=expires_at)

        token = generated_link.split('/')[-1]

        now = timezone.now()
        timezone.now = Mock()
        timezone.now.return_value = now + timedelta(seconds=400)

        process_image_service = ProcessImageService(self.image, token)
        self.assertRaises(ExpiredLinkException, process_image_service.process)

