from tempfile import TemporaryDirectory
from typing import List, Dict
from django.test import TestCase, override_settings

from accounts.models import Plan, User
from accounts.tests.factories import UserFactory
from core.exceptions import UserDoesNotHavePlan
from core.tests.factories import ImageFactory
from core.services.user_images import UserImagesService


@override_settings(MEDIA_ROOT=TemporaryDirectory(prefix='media_test').name)
class UserImagesServiceTestCase(TestCase):
    fixtures = ['accounts/fixtures/plans.json', ]

    @classmethod
    def setUpTestData(cls):
        cls.basic_plan = Plan.objects.get(name='Basic')
        cls.premium_plan = Plan.objects.get(name='Premium')
        cls.enterprise_plan = Plan.objects.get(name='Enterprise')
        cls.user = UserFactory.create()

    def setUp(self):
        for i in range(3):
            ImageFactory.create(user=self.user)

    def test_user_with_no_plan(self):
        user_images_service = UserImagesService(user=self.user)
        self.assertRaises(UserDoesNotHavePlan, user_images_service.get_images)

    def check_image_links_with_given_plan(self, image_links: List[Dict], plan: Plan):
        """A helper method to check generated links with associated user plan"""
        for image in image_links:
            self.assertEqual('original_image' in image, plan.has_original_image)

            for size in plan.image_sizes:
                self.assertTrue(f'size_{size}' in image)

    def test_generated_link_keys(self):
        user_images_service = UserImagesService(user=self.user)
        # test image links of user with basic plan
        self.user.account_plan = self.basic_plan
        self.user.save()
        basic_plan_images = user_images_service.get_images()
        self.check_image_links_with_given_plan(basic_plan_images, self.basic_plan)

        # test image links of user with premium plan
        self.user.account_plan = self.premium_plan
        self.user.save()
        premium_plan_images = user_images_service.get_images()
        self.check_image_links_with_given_plan(premium_plan_images, self.premium_plan)

        # test image links of user with enterprise plan
        self.user.account_plan = self.enterprise_plan
        self.user.save()
        enterprise_plan_images = user_images_service.get_images()
        self.check_image_links_with_given_plan(enterprise_plan_images, self.enterprise_plan)