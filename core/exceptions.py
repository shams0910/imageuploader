from rest_framework.exceptions import APIException


class UndefinedException(APIException):
    status_code = 400
    default_detail = 'Something went wrong'
    default_code = 'undefined_error'


class UserDoesNotHavePlan(APIException):
    status_code = 400
    default_detail = 'User does not have active plan'
    default_code = 'user_does_not_have_plan'


class InValidUrl(APIException):
    status_code = 400
    default_detail = 'Something is wrong with this link'
    default_code = 'invalid_url'


class ExpiredLinkException(APIException):
    status_code = 400
    default_detail = 'Link to image expired'
    default_code = 'expired_link'
