from datetime import timedelta
from wsgiref.util import FileWrapper

from django.core.files.images import ImageFile
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework.renderers import JSONRenderer
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.generics import GenericAPIView, DestroyAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from core.exceptions import ExpiredLinkException, UndefinedException
from core.models import Image
from core.permissions import CanRequestExpiringLink, ImageOwner
from core.renderers import JPEGRenderer, PNGRenderer
from core.serializers import ImageSerializer, RequestExpiringLinkSerializer
from core.services.process_image import ProcessImageService
from core.services.user_images import UserImagesService

from django.http import HttpResponse

__all__ = ['UploadImageView']


class UploadImageView(GenericAPIView):
    parser_classes = [MultiPartParser]
    serializer_class = ImageSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user_id=2)
        return Response({})


class ImageListView(APIView):

    def get(self, request):
        user = request.user
        images = UserImagesService(user=user).get_images()
        return Response(images)


class DeleteImageView(DestroyAPIView):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = (IsAuthenticated, ImageOwner)


class DownloadImage(GenericAPIView):
    permission_classes = ()
    renderer_classes = [JPEGRenderer, PNGRenderer, JSONRenderer]

    def get(self, request, uidb64, token):
        image_id = force_text(urlsafe_base64_decode(uidb64))
        image_instance = get_object_or_404(Image, pk=image_id)

        try:
            processed_image = ProcessImageService(image_instance, token).process()
        except (ExpiredLinkException, ValidationError) as e:
            return Response(e.detail, content_type='application/json')
        except Exception:
            return Response(UndefinedException.default_detail, content_type='application/json')

        if isinstance(processed_image, ImageFile):
            response = Response(FileWrapper(processed_image))
            response['Content-Disposition'] = 'attachment; filename=%s' % image_instance.filename
            return response

        response = HttpResponse()
        response['Content-Disposition'] = 'attachment; filename=%s' % image_instance.filename
        processed_image.save(response, processed_image.format)
        return response


class RequestExpiringImageLink(GenericAPIView):
    serializer_class = RequestExpiringLinkSerializer
    permission_classes = (IsAuthenticated, CanRequestExpiringLink)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        expires_at = timezone.now() + timedelta(seconds=serializer.validated_data['seconds'])

        if serializer.image_instance.user != request.user:
            raise PermissionDenied('The requested image does not belong to you')

        expiring_image_links = UserImagesService.get_image(
            image=serializer.image_instance,
            plan=request.user.account_plan,
            expires_at=expires_at
        )

        return Response(expiring_image_links)
