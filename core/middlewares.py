from django.utils import timezone


class TimezoneMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        request_tz = request.headers.get('timezone', 'Europe/Warsaw')
        timezone.activate(request_tz)

        response = self.get_response(request)

        return response
