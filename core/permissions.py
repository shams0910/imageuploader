from rest_framework.permissions import BasePermission

from accounts.models import Plan
from core.models import Image


class UserPlanRequired(BasePermission):
    def has_permission(self, request, view):
        if request.user.account_plan:
            return True
        else:
            return False


class CanRequestExpiringLink(BasePermission):
    def has_permission(self, request, view):
        user_plan: Plan = request.user.account_plan
        if user_plan.expiring_links:
            return True
        else:
            return False


class ImageOwner(BasePermission):
    def has_permission(self, request, view):
        image: Image = view.get_object()
        if request.user == image.user:
            return True
        else:
            return False
