from django.urls import path

from core import views

urlpatterns = [
    path('list_images', views.ImageListView.as_view()),
    path('upload_image', views.UploadImageView.as_view()),
    path('download_image/<uidb64>/<token>', views.DownloadImage.as_view(), name='download_image'),
    path('request_expiring_image_link', views.RequestExpiringImageLink.as_view()),
    path('delete_image/<int:pk>', views.DeleteImageView.as_view())
]