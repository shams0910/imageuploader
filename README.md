## Image Uploader
API to upload images and get them in different sizes.\

**Live preview** [http://18.159.195.155/login](http://18.159.195.155/login) \
 **email**: `brown@gmail.com` **password**: `hello0909` 

### Setup

Clone the repository
```
git clone https://gitlab.com/shams0910/imageuploader.git
```

Start docker-compose

```
docker-compose up
```

Apply migrations
```
docker exec django python manage.py migrate
```

Load fixtures for plans (Basic, Premium, Enterprise plans are there).
```
docker exec django python manage.py loaddata plans
```

There is fixture for 2 predefined users (1 superuser and 1 regular user). \
Please load it after loading plan fixture, because the regular user has one of the plan as FK.\
Superuser: **email**: `shams@gmail.com` **password**: `hello0909` \
Regular user: **email**: `brown@gmail.com` **password**: `hello0909` 

```
docker exec django python manage.py loaddata users
```

However, if you want to create superuser yourself
```
docker exec -it django python manage.py createsuperuser
```

And add regular user with plan in [django admin panel](http://127.0.0.1:8007/admin).
Notice, user without plan can't have full usability

## APIs
### Login
Please first log in at `http://127.0.0.1:8007/login`.

### Upload image
Upload image at `http://127.0.0.1:8007/upload_image`.

### List images
List your images at `http://127.0.0.1:8007/list_images`.

There you get your images in the format bellow, the data will vary depending on your plan.
```json
[
    {
        "id": 3,
        "filename": "Screenshot_from_2021-11-06_11-51-51.png",
        "created_at": "08-11-2021 09:45",
        "original_image": "http://127.0.0.1:8007/download_image/Mw/eyJpc19vcml.....",
        "size_200": "http://127.0.0.1:8007/download_image/Mw/eyJpc19vcmlnaW5hb.....",
        "size_400": "http://127.0.0.1:8007/download_image/Mw/eyJpc19vc....."
    },
    {
        "id": 4,
        "filename": "karsten-winegeart-W9c6YicHKyE-unsplash.jpg",
        "created_at": "08-11-2021 10:08",
        "original_image": "http://127.0.0.1:8007/download_image/NA/eyJp.....",
        "size_200": "http://127.0.0.1:8007/download_image/NA/eyJpc19.....",
        "size_400": "http://127.0.0.1:8007/download_image/NA/eyJp....."
    }
]
```
You can click the appropriate image link you want, and you will be able to download it.

### Get expiring image link
Go to `http://127.0.0.1:8007/request_expiring_image_link`. 
There you will see the form below. Please enter there id of the image and the number of seconds for expiring link. \
![request_expiring_image_link](/docs/images/request_expiring_image_link.png)

### Delete your image
The endpoint is `http://127.0.0.1:8007/delete_image/id_here`

\
***There you go! Thank you for attention***
\

