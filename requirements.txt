django===3.2
djangorestframework===3.12.1
djangorestframework-simplejwt==4.8.0
django-cors-headers==3.5.0
pillow==8.1.2
psycopg2==2.8.6
factory_boy==3.2.0